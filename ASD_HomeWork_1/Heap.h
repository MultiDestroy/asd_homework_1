#pragma once
#include <iostream>
#include "Utils.h"
using namespace std;

class Heap {
private:
	int* arr;
	int capacity;
	int currSize;
	bool allocated;

	int parent(int i);
	int leftChild(int i);
	int rightChild(int i);
	void siftUp(int i);

public:
	Heap(int capacity);
	Heap(int* arr, int size);
	~Heap();

	void heapify(int size, int i);
	void insert(int val);
	int extractMax();
	static Heap buildHeap(int* array, int size);
};


Heap Heap::buildHeap(int* array, int size)
{
	Heap heap(array, size);
	for (int i = size / 2 - 1; i >= 0; i--)
		heap.heapify(size, i);

	return heap;
}

Heap::Heap(int capacity)
{
	this->arr = new int[capacity];
	this->capacity = capacity;
	this->currSize = 0;
	this->allocated = true;
}

Heap::Heap(int* arr, int size)
{
	this->arr = arr;
	this->currSize = size;
	this->capacity = size;
	this->allocated = false;
}

Heap::~Heap()
{
	if (allocated)
		delete[] arr;
}

void Heap::heapify(int size, int i)
{
	int left = leftChild(i);
	int right = rightChild(i);
	int max = i;

	if (left < size && arr[left] > arr[i])
		max = left;

	if (right < size && arr[right] > arr[max])
		max = right;

	if (max != i)
	{
		Utils::swap(&arr[i], &arr[max]);
		heapify(size, max);
	}

}

void Heap::insert(int val)
{
	if (currSize < capacity)
	{
		currSize++;
		arr[currSize - 1] = val;
		siftUp(currSize - 1);
	}
}

int Heap::extractMax()
{
	if (currSize > 0)
		return arr[0];
	else
		return -1;
}

int Heap::parent(int i)
{
	if (i == 0)
		return 0;

	return (i - 1) >> 1;
}

int Heap::leftChild(int i)
{
	return (i << 1) + 1;
}

int Heap::rightChild(int i)
{
	return (i << 1) + 2;
}

void Heap::siftUp(int i)
{
	int parent = this->parent(i);

	if (arr[parent] > 0) {
	
		if (arr[i] > arr[parent]) {
			Utils::swap(&arr[i], &arr[parent]);

			siftUp(parent);
		}
	}
}