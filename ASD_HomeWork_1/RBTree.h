#pragma once
#include <assert.h>
#include "BSTTree.h"
//�r�d�a:
//https://www.geeksforgeeks.org/red-black-tree-set-2-insert/?ref=lbp
//https://www.coders-hub.com/2015/07/red-black-tree-rb-tree-using-c.html#.WmauxHnhXIU
//https://github.com/Bibeknam/algorithmtutorprograms/blob/master/data-structures/red-black-trees/RedBlackTree.cpp

class RBTree : public BSTTree
{
public:
	enum Color {BLACK, RED};
	struct Node;
	Node* NIL;
	RBTree();
	~RBTree();
	void insert(int key);
	void deleteNode(int key);
private:
	bool bstInsert(Node* node);
	void deleteNode(Node* root, int key);
	void leftRotation(Node* node);
	void rightRotation(Node* node);
	void fixAfterInsert(Node* node);
	void fixAfterDelete(Node* node);
	void rbTransplant(Node* u, Node* v);
	Node* minimum(Node* node);
	void destroyRecursive(Node* node);
	std::string inOrder();
	std::string preOrder();
	void inOrder(Node* root, std::string* order);
	void preOrder(Node* root, std::string* order);
};

//Struktura w�z�a

struct RBTree::Node : public Tree::Node
{
	Node* parent = nullptr;
	enum Color color = RED;

	Node(int key) : BSTTree::Node(key) {};
	Node* getLeft();
	Node* getRight();
};

RBTree::Node* RBTree::Node::getLeft() {
	return (Node*)this->left;
}

RBTree::Node* RBTree::Node::getRight() {
	return (Node*)this->right;
}

//Implementacja insert i delete

bool RBTree::bstInsert(Node* node)
{
	Node* x = (Node*) treeRoot;
	Node* y = nullptr;
	while (x != NIL) {
		y = x;
		if (node->key < x->key) {
			x = x->getLeft();
		}
		else {
			x = x->getRight();
		}
	}

	// Y jest ojcem X
	node->parent = y;
	if (y == nullptr) {
		treeRoot = node;
	}
	else if (node->key < y->key) {
		y->setLeft(node);
	}
	else {
		y->setRight(node);
	}

	// if new node is a root node, simply return
	if (node->parent == nullptr) {
		node->color = BLACK;
		return false;
	}

	// if the grandparent is null, simply return
	if (node->parent->parent == nullptr) {
		return false;
	}
	
	//Tree needs to be fixed
	return true;
}

void RBTree::deleteNode(Node* node, int key)
{
	Node* toDel = NIL;
	Node* y;
	Node* x;

	//Znajd� element
	while (node != NIL) {
		if (node->key == key) {
			toDel = node;
		}

		if (node->key <= key) {
			node = node->getRight();
		}
		else {
			node = node->getLeft();
		}
	}

	//Brak elementu w drzewie
	if (toDel == NIL)
		return;

	y = toDel;
	Color y_originalColor = y->color;
	if (toDel->getLeft() == NIL)
	{
		x = toDel->getRight();
		rbTransplant(toDel, toDel->getRight());
	}
	else if (toDel->getRight() == NIL)
	{
		x = toDel->getLeft();
		rbTransplant(toDel, toDel->getLeft());
	}
	else
	{
		y = minimum(toDel->getRight());
		y_originalColor = y->color;
		x = y->getRight();
		if (y->parent == toDel)
		{
			x->parent = y;
		}
		else
		{
			rbTransplant(y, y->getRight());
			y->setRight(toDel->getRight());
			y->getRight()->parent = y;
		}

		rbTransplant(toDel, y);
		y->setLeft(toDel->getLeft());
		y->getLeft()->parent = y;
		y->color = toDel->color;
	}
	delete toDel;
	if (y_originalColor == BLACK)
		fixAfterDelete(x);
}

void RBTree::insert(int key)
{
	Node* newNode = new Node(key);
	newNode->setLeft(NIL);
	newNode->setRight(NIL);

	//Wstaw do drzewa tak samo jak w BST
	if (bstInsert(newNode));
		fixAfterInsert(newNode); //Napraw drzewo
}

void RBTree::deleteNode(int key)
{
	deleteNode((Node*)treeRoot, key);
}

void RBTree::rbTransplant(Node* u, Node* v) {
	if (u->parent == nullptr) {
		treeRoot = v;
	}
	else if (u == u->parent->getLeft()) {
		u->parent->setLeft(v);
	}
	else {
		u->parent->setRight(v);
	}

	v->parent = u->parent;
}

RBTree::Node* RBTree::minimum(Node* node)
{
	while (node->getLeft() != NIL) {
		node = node->getLeft();
	}
	return node;
}

//Implementacja metod naprawiaj�cych drzewo

void RBTree::fixAfterInsert(Node* node)
{
	while (node->parent != nullptr && node->parent->color == RED)
	{
		//Ojciec jest lewym dzieckiem
		if (node->parent == node->parent->parent->getLeft())
		{
			Node* uncle = node->parent->parent->getRight();
			//Wujek jest czerwonym w�z�em
			if (uncle->color == RED)
			{
				node->parent->color = BLACK;
				uncle->color = BLACK;
				node->parent->parent->color = RED;
				node = node->parent->parent;
			}
			else
			{
				//Przypadek 2
				//W�ze� jest prawym dzieckiem
				//Lewa rotacja
				if (node == node->parent->getRight())
				{
					node = node->parent;
					leftRotation(node);
				}

				//Przypadek 3
				//W�ze� jest lewym dzieckiem
				//Prawa rotacja
				node->parent->color = BLACK;
				node->parent->parent->color = RED;
				rightRotation(node->parent->parent);
			}
		}
		else
		{
			Node* uncle = node->parent->parent->getLeft();
			//Wujek jest czerwonym w�z�em
			if (uncle->color == RED)
			{
				node->parent->color = BLACK;
				uncle->color = BLACK;
				node->parent->parent->color = RED;
				node = node->parent->parent;
			}
			else
			{
				//Przypadek 2
				//W�ze� jest lewym dzieckiem
				//Prawa rotacja
				if (node == node->parent->getLeft())
				{
					node = node->parent;
					rightRotation(node);
				}

				//Przypadek 3
				//W�ze� jest prawym dzieckiem
				//Lewa rotacja
				node->parent->color = BLACK;
				node->parent->parent->color = RED;
				leftRotation(node->parent->parent);
			}
		}
		if (node == treeRoot)
			break;
	}
	((Node*)treeRoot)->color = BLACK;
}

void RBTree::fixAfterDelete(Node* node)
{
	Node* sibling;
	while (node != treeRoot && node->color == BLACK)
	{
		//Je�li w�ze� jest lewym dzieckiem
		if (node == node->parent->getLeft())
		{
			sibling = node->parent->getRight();
			//Je�li brat jest czerwonym w�z�em
			if (sibling->color == RED)
			{
				sibling->color = BLACK;
				node->parent->color = RED;
				leftRotation(node->parent);
				sibling = node->parent->getRight();
				}
			//Je�li oboje dzieci brata s� w�z�ami czarnymi
			if (sibling->getRight()->color == BLACK && sibling->getLeft()->color == BLACK)
			{
				sibling->color = RED;
				node = node->parent;
			}
			//W przeciwnym przypadku
			else
			{
				if (sibling->getRight()->color == BLACK)
				{
					sibling->getLeft()->color = BLACK;
					sibling->color = RED;
					rightRotation(sibling);
					sibling = node->parent->getRight();
				}
				sibling->color = node->parent->color;
				node->parent->color = BLACK;
				sibling->getRight()->color = BLACK;
				leftRotation(node->parent);
				node = (Node*) treeRoot;
			}
		}
		//Je�li w�ze� jest prawym dzieckiem
		else
		{
			sibling = node->parent->getLeft();
			//Je�li brat jest czerwonym w�z�em
			if (sibling->color == RED)
			{
				sibling->color = BLACK;
				node->parent->color = RED;
				rightRotation(node->parent);
				sibling = node->parent->getLeft();
			}
			//Je�li oboje dzieci brata s� w�z�ami czarnymi
			if (sibling->getLeft()->color == BLACK && sibling->getRight()->color == BLACK)
			{
				sibling->color = RED;
				node = node->parent;
			}
			//W przeciwnym przypadku
			else
			{
				if (sibling->getLeft()->color == BLACK)
				{
					sibling->getRight()->color = BLACK;
					sibling->color = RED;
					leftRotation(sibling);
					sibling = node->parent->getLeft();
				}
				sibling->color = node->parent->color;
				node->parent->color = BLACK;
				sibling->getLeft()->color = BLACK;
				rightRotation(node->parent);
				node = (Node*) treeRoot;
			}
		}
	}
	node->color = BLACK;
}

//Rotacje

void RBTree::leftRotation(Node* node)
{	
	Node* y = node->getRight();
	node->setRight(y->getLeft());
	if (y->getLeft() != NIL) {
		y->getLeft()->parent = node;
	}
	y->parent = node->parent;
	if (node->parent == nullptr) {
		treeRoot = y;
	}
	else if (node == node->parent->getLeft()) {
		node->parent->setLeft(y);
	}
	else {
		node->parent->setRight(y);
	}
	y->setLeft(node);
	node->parent = y;
}

void RBTree::rightRotation(Node* node)
{
	Node* y = node->getLeft();
	node->setLeft(y->getRight());
	if (y->getRight() != NIL) {
		y->getRight()->parent = node;
	}
	y->parent = node->parent;
	if (node->parent == nullptr) {
		treeRoot = y;
	}
	else if (node == node->parent->getRight()) {
		node->parent->setRight(y);
	}
	else {
		node->parent->setLeft(y);
	}
	y->setRight(node);
	node->parent = y;
}

//Konstruktor i destruktor

RBTree::RBTree()
{
	NIL = new Node(-1);
	NIL->color = BLACK;
	NIL->setLeft(nullptr);
	NIL->setRight(nullptr);
	treeRoot = NIL;
}

RBTree::~RBTree()
{
	destroyRecursive((Node*) treeRoot);
	delete NIL;
	treeRoot = nullptr;
}

void RBTree::destroyRecursive(Node* node)
{
	if (node != nullptr && node != NIL)
	{
		destroyRecursive(node->getLeft());
		destroyRecursive(node->getRight());
		delete node;
	}
}

//InOrder i PreOrder

void RBTree::inOrder(Node* root, std::string* order)
{
	if (root == nullptr || root == NIL)
		return;

	inOrder(root->getLeft(), order);
	*order += std::to_string(root->key) + " ";
	inOrder(root->getRight(), order);
}

void RBTree::preOrder(Node* root, std::string* order)
{
	if (root != nullptr && root != NIL)
	{
		*order += std::to_string(root->key) + " ";
		preOrder(root->getLeft(), order);
		preOrder(root->getRight(), order);
	}
}

std::string RBTree::inOrder()
{
	std::string order;
	inOrder((Node*) treeRoot, &order);
	return order;
}

std::string RBTree::preOrder()
{
	std::string order;
	preOrder((Node*) treeRoot, &order);
	return order;
}