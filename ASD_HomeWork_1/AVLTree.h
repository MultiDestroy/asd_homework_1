#pragma once
#include <math.h>
#include "BSTTree.h"
//�r�d�a:
//https://www.geeksforgeeks.org/avl-tree-set-1-insertion/
//https://www.geeksforgeeks.org/avl-tree-set-2-deletion/
//Prezentacja z zaj��

class AVLTree : public BSTTree
{
public:
	struct Node;
private:
	int getBalance(Node* node);

	Node* leftRotation(Node* node);
	Node* rightRotation(Node* node);

	Node* insert(Node* root, int key);
	Node* deleteNode(Node* root, int key);
};

//Strukutra w�z�a

struct AVLTree::Node : public Tree::Node
{
	int height = 1;
	int getHeight();
	Node(int key) : Tree::Node(key) {};
	Node* getLeft();
	Node* getRight();
};

AVLTree::Node* AVLTree::Node::getLeft() {
	return (Node*)this->left;
}

AVLTree::Node* AVLTree::Node::getRight() {
	return (Node*)this->right;
}

int AVLTree::Node::getHeight()
{
	return this == nullptr ? 0 : height;
}

//Implementacja insert i delete

AVLTree::Node* AVLTree::insert(Node* root, int key)
{
	//Wk�adanie elementu dok�adnie takie samo jak w drzewie BST
	if (root == nullptr)
		return new Node(key);

	if (key < root->key)
		root->setLeft(insert(root->getLeft(), key));
	else if (key > root->key)
		root->setRight(insert(root->getRight(), key));
	else //Nie zezwalamy na umieszczanie element�w z tym samym kluczem
		return root;
	
	//Zwiekszmy wysoko�� po wstawieniu
	root->height = 1 + std::max(root->getLeft()->getHeight(), root->getRight()->getHeight());
	
	int balance = getBalance(root);
		
	//Rotacja LL
	if (balance > 1 && key < root->getLeft()->key)
		return rightRotation(root);

	//Rotacja RR 
	if (balance < -1 && key > root->getRight()->key)
		return leftRotation(root);

	//Rotacja LR
	if (balance > 1 && key > root->getLeft()->key)
	{
		root->setLeft(leftRotation(root->getLeft()));
		return rightRotation(root);
	}

	//Rotacja RL
	if (balance < -1 && key < root->getRight()->key)
	{
		root->setRight(rightRotation(root->getRight()));
		return leftRotation(root);
	}

	return root;
}

AVLTree::Node* AVLTree::deleteNode(Node* root, int key)
{
	//Normalne usuniecie elementu z drzewa (jak w BST)
	if (root == nullptr)
		return nullptr;

	//Szukaj w lewym poddrzewie
	if (key < root->key)
		root->setLeft(deleteNode(root->getLeft(), key));
	//Szukaj w prawym poddrzewie
	else if (key > root->key)
		root->setRight(deleteNode(root->getRight(), key));
	//Szukany element jest w roocie
	else
	{
		//W�ze� z jednym dzieckiem lub bez dzieci
		if ((root->getLeft() == nullptr) || (root->getRight() == nullptr))
		{
			Node* temp = root->getLeft() != nullptr ? root->getLeft() : root->getRight();

			// Przypadek bez dzieci
			if (temp == nullptr)
			{
				temp = root;
				root = nullptr;
			}
			else // Jedno dziecko 
				*root = *temp; 
								
			free(temp);
		}
		else
		{
			//W�ze� z dwoma dzie�mi
			//Znajd� najmiejszego nast�pce
			Node* temp = (Node*) minValueNode(root->getRight());

			//Przypisz klucz nast�pcy do korzenia
			root->key = temp->key;

			//Usu� nast�pce
			root->setRight(deleteNode(root->getRight(), temp->key));
		}
	}

	if (root == NULL)
		return root;

	//Zmie� wysoko�� w�z�a
	root->height = 1 + std::max(root->getLeft()->getHeight(), root->getRight()->getHeight());

	int balance = getBalance(root);

	//Rotacja LL
	if (balance > 1 && getBalance(root->getLeft()) >= 0)
		return rightRotation(root);

	//Rotacja LR 
	if (balance > 1 && getBalance(root->getLeft()) < 0)
	{
		root->setLeft(leftRotation(root->getLeft()));
		return rightRotation(root);
	}

	//Rotacja RR
	if (balance < -1 && getBalance(root->getRight()) <= 0)
		return leftRotation(root);

	//Rotacja RL  
	if (balance < -1 && getBalance(root->getRight()) > 0)
	{
		root->setRight(rightRotation(root->getRight()));
		return leftRotation(root);
	}

	return root;
}

//Rotacje

AVLTree::Node* AVLTree::leftRotation(Node* node)
{
	Node* right = node->getRight();
	Node* toSwap = right->getLeft();

	right->setLeft(node);
	node->setRight(toSwap);

	node->height = std::max(node->getLeft()->getHeight(), node->getRight()->getHeight()) + 1;
	right->height = std::max(right->getLeft()->getHeight(), right->getRight()->getHeight()) + 1;

	return right;
}

AVLTree::Node* AVLTree::rightRotation(Node* node)
{
	Node* left = node->getLeft();
	Node* toSwap = left->getRight();

	left->setRight(node);
	node->setLeft(toSwap);

	node->height = std::max(node->getLeft()->getHeight(), node->getRight()->getHeight()) + 1;
	left->height = std::max(left->getLeft()->getHeight(), left->getRight()->getHeight()) + 1;

	return left;
}

//Pozosta�e metody

int AVLTree::getBalance(Node* node)
{
	return node == nullptr ? 0 :
		node->getLeft()->getHeight() - node->getRight()->getHeight();
}