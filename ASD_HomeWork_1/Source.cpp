﻿#include <iostream>
#include "Algorithm.h"
#include "Utils.h"
#include "BSTTree.h"
#include "RBTree.h"
#include "AVLTree.h"
using namespace std;

void testInsertionSort(Algorithm algorithm);

void testMergeSort(Algorithm algorithm);

void testHeapSort(Algorithm algorithm);

void testTree(string treeName, Tree* tree);

int main()
{
	//Drzewa

	testTree("BSTTree", new BSTTree);
	testTree("AVLTree", new AVLTree);
	testTree("RBTree", new RBTree);

	//Sortowania
	cout << endl << "---- Sortowania ----" << endl;

	Algorithm algorithm;

    testInsertionSort(algorithm);
	testMergeSort(algorithm);
	testHeapSort(algorithm);
	cout << endl << "-----------------" << endl;
	return 0;

}

void testTree(string treeName, Tree* tree)
{
	cout << "---- " << treeName << " ----" << endl;

	//Tworzę drzewo
	tree->insert(4);
	tree->insert(3);
	tree->insert(1);
	tree->insert(7);
	tree->insert(10);
	tree->insert(9);
	tree->insert(16);

	cout << "Przykladowe drzewo (INORDER): " + tree->inOrder() << endl;

	tree->deleteNode(4);
	cout << "Usuwam 4 || " << tree->inOrder() << endl;;

	tree->deleteNode(16);
	cout << "Usuwam 16 || " << tree->inOrder() << endl;

	tree->deleteNode(100);
	cout << "Usuwam 100 (nie istnieje) || " + tree->inOrder() << endl;

	tree->insert(2);
	cout << "Wstawiam 2 || " + tree->inOrder() << endl;

	tree->deleteNode(9);
	cout << "Usuwam 9 || " +  tree->inOrder() << endl;

	tree->deleteNode(10);
	cout << "Usuwam 10 || " + tree->inOrder() << endl;

	tree->insert(11);
	cout << "Wstawiam 11 || " + tree->inOrder() << endl;
	delete tree;

	cout << "-----------------" << endl << endl;
}

void testInsertionSort(Algorithm algorithm)
{
	cout << "InsertionSort test" << endl;

	//Testy ręczne
	//1
	int array[]{ 7,1,0,2,65,1,9,8, -1 };
	algorithm.insertionSort(array, 9);
	Utils::displayArray(array, 9);
	Utils::verify(array, 9);
	//2
	int array2[]{ 5,1,25,6,3,2 };
	algorithm.insertionSort(array2, 6);
	Utils::displayArray(array2, 6);
	Utils::verify(array2, 6);
	//3
	int array3[]{ 1,5,4 };
	algorithm.insertionSort(array3, 3);
	Utils::displayArray(array3, 3);
	Utils::verify(array3, 3);

	//Testy z generatora
	//1
	int size = 1000;
	int* arr = Utils::generateArray(1000, 0, 1000);
	algorithm.insertionSort(arr, size);
	Utils::verify(arr, size);
	delete[] arr;

	//2
	size = 555;
	arr = Utils::generateArray(555, 0, 1000);
	algorithm.insertionSort(arr, size);
	Utils::verify(arr, size);
	delete[] arr;

	//3
	size = 112;
	arr = Utils::generateArray(112, 0, 670);
	algorithm.insertionSort(arr, size);
	Utils::verify(arr, size);
	delete[] arr;
}

void testMergeSort(Algorithm algorithm)
{
	cout << "MergeSort test" << endl;

	//Testy ręczne
	//1
	int array[]{ 7,1,0,2,65,1,9,8, -1 };
	algorithm.mergeSort(array, 0, 8);
	Utils::displayArray(array, 9);
	Utils::verify(array, 9);
	//2
	int array2[]{ 5,1,25,6,3,2 };
	algorithm.mergeSort(array2, 0, 5);
	Utils::displayArray(array2, 6);
	Utils::verify(array2, 6);
	//3
	int array3[]{ 1,5,4 };
	algorithm.mergeSort(array3, 0, 2);
	Utils::displayArray(array3, 3);
	Utils::verify(array3, 3);

	//Testy z generatora
	//1
	int size = 1000;
	int* arr = Utils::generateArray(1000, 0, 1000);
	algorithm.mergeSort(arr, 0, size - 1);
	Utils::verify(arr, size);
	delete[] arr;

	//2
	size = 555;
	arr = Utils::generateArray(555, 0, 1000);
	algorithm.mergeSort(arr, 0, size - 1);
	Utils::verify(arr, size);
	delete[] arr;

	//3
	size = 112;
	arr = Utils::generateArray(112, 0, 670);
	algorithm.mergeSort(arr, 0, size - 1);
	Utils::verify(arr, size);
	delete[] arr;
}

void testHeapSort(Algorithm algorithm)
{
	cout << "HeapSort test" << endl;

	//Testy ręczne
	//1
	int array[]{ 7,1,0,2,65,1,9,8, -1 };
	algorithm.heapSort(array, 9);
	Utils::displayArray(array, 9);
	Utils::verify(array, 9);
	//2
	int array2[]{ 5,1,25,6,3,2 };
	algorithm.heapSort(array2, 6);
	Utils::displayArray(array2, 6);
	Utils::verify(array2, 6);
	//3
	int array3[]{ 1,5,4 };
	algorithm.heapSort(array3, 3);
	Utils::displayArray(array3, 3);
	Utils::verify(array3, 3);

	//Testy z generatora
	//1
	int size = 1000;
	int* arr = Utils::generateArray(1000, 0, 1000);
	algorithm.heapSort(arr, size);
	Utils::verify(arr, size);
	delete[] arr;

	//2
	size = 555;
	arr = Utils::generateArray(555, 0, 1000);
	algorithm.heapSort(arr, size);
	Utils::verify(arr, size);
	delete[] arr;

	//3
	size = 112;
	arr = Utils::generateArray(112, 0, 670);
	algorithm.heapSort(arr, size);
	Utils::verify(arr, size);
	delete[] arr;
}