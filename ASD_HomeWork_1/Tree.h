#pragma once
#include <iostream>
#include <string>

class Tree
{
public:
	struct Node;
	virtual void insert(int key);
	virtual void deleteNode(int key);
	virtual Node* search(int key);
	virtual std::string inOrder();
	virtual std::string preOrder();
	virtual ~Tree();

protected:
	Node* treeRoot = nullptr;
	virtual Node* insert(Node* node, int key) = 0;
	virtual Node* deleteNode(Node* node, int key) = 0;
	virtual Node* minValueNode(Node* root);

private:
	virtual Node* search(Node* node, int key);
	virtual void inOrder(Node* root, std::string* order);
	virtual void preOrder(Node* root, std::string* order);
	virtual void destroyRecursive(Node* root);
};

//Strukura Node

struct Tree::Node
{
protected:
	Node* left = nullptr;
	Node* right = nullptr;

public:
	int key;
	Node(int);
	void setLeft(Node*);
	Node* getLeft();
	void setRight(Node*);
	Node* getRight();
};

Tree::Node::Node(int key)
{
	this->key = key;
}

void Tree::Node::setLeft(Node* node) {
	this->left = node;
}

Tree::Node* Tree::Node::getLeft() {
	return this->left;
}

void Tree::Node::setRight(Node* node) {
	this->right = node;
}

Tree::Node* Tree::Node::getRight() {
	return this->right;
}

//Implementacja search, insert i delete

void Tree::insert(int key)
{
	treeRoot = insert(treeRoot, key);
}

void Tree::deleteNode(int key)
{
	treeRoot = deleteNode(treeRoot, key);
}

Tree::Node* Tree::search(int key)
{
	return search(treeRoot, key);
}

//Przechodzenia drzewa

std::string Tree::inOrder()
{
	std::string order;
	inOrder(treeRoot, &order);
	return order;
}

void Tree::inOrder(Node* root, std::string* order)
{
	if (root == nullptr)
		return;

	inOrder(root->getLeft(), order);
	*order += std::to_string(root->key) + " ";
	inOrder(root->getRight(), order);
}

std::string Tree::preOrder()
{
	std::string order;
	preOrder(treeRoot, &order);
	return order;
}

void Tree::preOrder(Node* root, std::string* order)
{
	if (root != nullptr)
	{
		*order += std::to_string(root->key) + " ";
		preOrder(root->getLeft(), order);
		preOrder(root->getRight(), order);
	}
}

//Pozosta�e metody

Tree::Node* Tree::minValueNode(Node* root)
{
	Node* current = root;

	//Przeszukuj ca�y czas lewe poddrzewa a� do momentu natrafienia na li��
	while (current && current->getLeft() != nullptr)
		current = current->getLeft();

	return current;
}

Tree::Node* Tree::search(Node* root, int key)
{
	//Je�li korze� nie jest pusty i jednocze�nie jego klucz nie jest tym kt�ry mieli�my znale�� to szukaj dalej
	if (root != nullptr && root->key != key)
	{
		//Klucz w roocie jest wi�kszy od szukanego, wi�c szukaj w prawym poddrzewie
		if (root->key < key)
			return search(root->getRight(), key);

		//Klucz w roocie jest mniejszy od szukanego wi�c szukaj w lewym poddrzewie
		return search(root->getRight(), key);
	}
	return root;
}

//Destruktor

Tree::~Tree()
{
	destroyRecursive(treeRoot);
}

void Tree::destroyRecursive(Node* node)
{
	if (node != nullptr)
	{
		destroyRecursive(node->getLeft());
		destroyRecursive(node->getRight());
		delete node;
	}
}








