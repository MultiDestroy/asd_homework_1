#pragma once
#include "Tree.h"
//�r�d�a:
//https://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/
//https://www.geeksforgeeks.org/binary-search-tree-set-2-delete/

class BSTTree : public Tree {
protected:
	Node* deleteNode(Node* root, int key);
	Node* insert(Node* root, int key);
};

Tree::Node* BSTTree::deleteNode(Node* root, int key)
{
	if (root == nullptr)
		return root;

	//Je�li klucz jest mniejszy od korzenia to szukaj elementu w lewym poddrzewie
	if (key < root->key)
		root->setLeft(deleteNode(root->getLeft(), key));

	//Je�li klucz jest mniejszy od korzenia to szukaj elementu w prawym poddrzewie
	else if (key > root->key)
		root->setRight(deleteNode(root->getRight(), key));

	//Je�li klucz jest taki sam jak w korzeniu, to nale�y usun�� korze�
	else {
		//W�ze� z jednym lub zeroma dzie�mi
		if (root->getLeft() == nullptr) {
			Node* temp = root->getRight();
			delete root;
			return temp;
		}
		else if (root->getRight() == nullptr) {
			Node* temp = root->getLeft();
			delete root;
			return temp;
		}

		//W�ze� z dwoma dzie�mi
		//Znajd� najmniejsz� warto�� z prawego poddrzewa (nast�pca)
		Node* temp = minValueNode(root->getRight());

		//Przypisz warto�� nast�pcy do tego korzenia
		root->key = temp->key;

		//Usu� z drzewa nast�pc�
		root->setRight(deleteNode(root->getRight(), temp->key));
	}
	return root;
}

Tree::Node* BSTTree::insert(Node* root, int key)
{
	//Je�li korze� jest pusty, umie�� nowy w�ze� w tym miejscu
	if (root == nullptr)
		return new Node(key);

	//Je�li klucz jest wi�kszy od korzenia to szukaj miejsca w prawym poddrzewie gdzie mo�emy go umie�ci�
	if (key > root->key)
		root->setRight(insert(root->getRight(), key));
	//Je�li klucz jest mniejszy od korzenia to szukaj miejsca w lewym poddrzewie gdzie mo�emy go umie�ci�
	else if (key < root->key)
		root->setLeft(insert(root->getLeft(), key));

	return root;
}