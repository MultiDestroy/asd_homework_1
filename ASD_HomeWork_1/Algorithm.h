#pragma once
#include "Heap.h"
#include "Utils.h"


class Algorithm
{
public:

	void insertionSort(int* array, int size);

	void mergeSort(int arr[], int p, int q);

	void heapSort(int* array, int size);
		
private:
	
	void merge(int arr[], int p, int q, int r);

};


void Algorithm::heapSort(int* array, int size)
{

	Heap heap = Heap::buildHeap(array, size);
	for (int i = size - 1; i >= 1; i--)
	{
		//cout << array[0] << array[i] << endl;
		Utils::swap(&array[0], &array[i]);
		//cout << array[0] << array[i] << endl << endl;
		size--;
		heap.heapify(size, 0);
	}
}

void Algorithm::insertionSort(int* array, int size)
{
	for (int j = 1; j < size; j++)
	{
		int element = array[j];
		int i = j - 1;
		while (i >= 0 && array[i] > element)
		{
			array[i + 1] = array[i];
			i--;
		}
		array[i + 1] = element;
	}
}

void Algorithm::mergeSort(int arr[], int p, int r)
{
	if (p < r)
	{
		int q = (p + r) / 2;
		mergeSort(arr, p, q);
		mergeSort(arr, q + 1, r);
		merge(arr, p, q, r);
	}
}

void Algorithm::merge(int arr[], int p, int q, int r)
{
	//Wielko�ci pomocniczych tablic
	int n1 = q - p + 1;
	int n2 = r - q;

	//Pomocnicze tablice
	int* left = new int[n1];
	int* right = new int[n2];

	//Przepisujemy elementy z g��wnej tablicy, do tablic pomocniczych
	for (int i = 0; i < n1; i++)
		left[i] = arr[p + i];
	for (int j = 0; j < n2; j++)
		right[j] = arr[q + 1 + j];

	//Indeksy do iterowania po tablicy
	int i = 0; //left
	int j = 0; //right
	int k = p; //array

	//Por�wnaj elementy i zamie� je miejscami
	while (i < n1 && j < n2)
	{
		if (left[i] <= right[j])
			arr[k] = left[i++];
		else
			arr[k] = right[j++];

		k++;
	}

	//Przepisujemy reszt� element�w
	while (i < n1)
		arr[k++] = left[i++];

	while (j < n2)
		arr[k++] = right[j++];

	//Zwalniamy pami�� zajmowan� przez pomocnicze tablice
	delete[] left;
	delete[] right;
}

