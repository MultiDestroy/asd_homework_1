#pragma once
#include <iostream>
#include <cstdlib>
#include <time.h> 
class Utils
{
public:
	Utils();
	~Utils();

	static int* generateArray(int size, int min, int max);
	static bool verify(int* array, int size);
	static void displayArray(int* array, int size);
	static void swap(int* a, int* b);
		
};

void Utils::swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

Utils::Utils()
{
	srand(time(NULL));
}

Utils::~Utils()
{
}

bool Utils::verify(int* array, int size)
{
	if (size > 0)
	{
		int old = array[0];
		for (int i = 1; i < size; i++)
		{
			if (old <= array[i])
				old = array[i];
			else {
				std::cout << "The array is unsorted!" << std::endl;
				return false;
			}
		}
		std::cout << "The array is sorted!" << std::endl;
		return true;
	}
	else
		std::cout << "The array size is 0!" << std::endl;
	return false;
}

int* Utils::generateArray(int size, int min, int max)
{

	int* array = new int[size];
	for (int i = 0; i < size; i++)
		array[i] = rand() % (max - min + 1) + min;

	return array;
}

void Utils::displayArray(int* array, int size)
{
	for (int i = 0; i < size; i++)
		std::cout << array[i] << " ";
	std::cout << std::endl;
}